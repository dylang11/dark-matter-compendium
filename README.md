![Supported Foundry Version(s)](https://img.shields.io/endpoint?url=https://foundryshields.com/version?url=https://gitlab.com/dark-matter1/dark-matter-compendium/raw/master/module.json)

# Dark Matter Compendiums
An add-on module of the [Dark Matter Core Rulebook](https://darkmatter.magehandpress.com/), created by [Mage Hand Press](https://store.magehandpress.com/) for use with the [Foundry Virtual Table Top](https://foundryvtt.com/) system.

## Installation
1. From within Foundry VTT Configuration and Setup Screen, select the Add-on Modules option.
2. Click on Install Module, and see the _Dependencies_ section below.
3. Copy the link for the Manafest URL (https://gitlab.com/dark-matter1/dark-matter-compendium/raw/master/module.json) into the same field at the bottom of the window.
4. Create a game world or open an existing world and enable the module under Game Settings > Manage Modules.

## Dependencies
This module _requires_ some other extensions to run properly.

* [Dark Matter System Extensions](https://gitlab.com/dark-matter1/dme) adds Dark Matter specific skills, weapon properties and more on top of the [Foundry VTT dnd5e Game System](https://gitlab.com/foundrynet/dnd5e).
* [game-icons.net](https://github.com/datdamnzotz/icons/blob/master/README-FoundryVTT.md) adds icons that we used with several item/spell icons. (Typically _White_ icons for Utility and _Black_ icons for Damaging/Offensive items.)
* _(optional)_ [Compendium Folders](https://github.com/earlSt1/vtt-compendium-folders) adds subfolders to the Compendiums menu. Makes it easier to search through all the system compendiums, but is NOT required to run.

**Note:** If you are running v0.8.x or higher of Foundry, installing the Dark Matter Compendium will also check to see if you have the necessary dependencies installed, and if not will prompt you to install them automatically.

## Getting Started
The main compendium entries are divided into categories like Race, Class, and Equipment. If you have [Compendium Folders](https://github.com/earlSt1/vtt-compendium-folders) module enabled they should all be in one folder called Dark Matter. If not you'll find them under Items or JournalEntry.

Most entries have links to other compendium entries that may simply be imported into your game world, or you may wish to drag and drop them into character sheets or (like in the case with Monsters), right into the active scene. For example, if you want to create an Amoeboid race player, start by opening the Dark Matter Races (Items) compendium and click on Amoeboid:

![](.gitlab/amoeboid-scn.png)

From here you can click on links within the Amoeboid entry to see other compendium entries like the text from the core Dark Matter book:

![](.gitlab/amoeboid-core-scn.png)

You can also drag the Amoeboid Race (Item) onto a blank Player Character sheet in Foundry to add the Amoeboid race stats to a new character.

![](.gitlab/amoeboid-pc-scn.png)

_(Notice how some of the Character Sheet fields fill in automatically?)_

# Special Thanks
- To [Mage Hand Press](https://mfov.magehandpress.com/) and the Digits of Vecna over on the [MHP Discord](https://discord.gg/pJEWa6b) for the amazing setting that is the Dark Matter 'Verse. (and for not shutting us down at first wind.)
- [@CSMcFarland#6471](https://gitlab.com/csmcfarland) for hosting @ gitlab, coding the heck out of the ['Verse](https://gitlab.com/dark-matter1/dme), and teaching Git to those eager to learn, but pretty clueless.
- [@Android8675#9424](https://gitlab.com/Android8675) who can't write anything here because it would be weird to talk about himself in the third person, but he's thankful that he's able to contribute so much.
- [@OwlbearAviary#9251](https://github.com/RabidOwlbear/) for the added html, javascript code foo and all the pretty colors.

## Notice:
We are currently looking for people to help on this project. If you think you'd like to help (even if you don't know anything about development or coding, etc), please feel free to reach out to one of the people mentioned here in Discord at the [Mage Hand Press Server](https://discord.gg/pJEWa6b).

# Contributing

You should be familiar with
[this process](https://foundryvtt.com/article/compendium/). Adding new content
to this compendium requires that you be a little familiar with git, with
copying files on the system where Foundry VTT is running, and with Foundry VTT
itself. You will also need a GitLab account. Bonus points if you are familiar
with [git lfs](https://docs.gitlab.com/ee/topics/git/lfs/).

## Setting Up
1. Clone this repository. You will be creating branches, pushing them, and
  opening merge requests.
2. Install the module into Foundry VTT where you are creating content.

## Creating Content
1. Create items, actors, scenes, etc.
2. Make sure the art you use is added to the images subdirectory of the module
  filesystem.
3. Unlock the corresponding compendium. There will be warnings.
4. Drag and drop your content to the compendium.

## Submitting The New Content
1. Create a new branch in the clone of this project.
2. Copy your module db files into the clone of this project.
3. Make sure any new artwork or audio files are also copied to the
   corresponding location in this project.
4. Add, commit, and push your changes.
5. Open a merge request in the GitLab web interface and assign it to someone.
